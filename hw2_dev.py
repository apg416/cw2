"""Aleksander Grzyb 01072956"""
"""M3C 2018 Homework 2"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize
from m1 import nmodel as nm #assumes that hw2_dev.f90 has been compiled with: f2py -c hw2_dev.f90 -m m1
# May also use scipy, scikit-learn, and time modules as needed
import time

def read_data(tsize=60000):
    """Read in image and label data from data.csv.
    The full image data is stored in a 784 x 70000 matrix, X
    and the corresponding labels are stored in a 70000 element array, y.
    The final 70000-tsize images and labels are stored in X_test and y_test, respectively.
    X,y,X_test, and y_test are all returned by the function.
    You are not required to use this function.
    """
    print("Reading data...") #may take 1-2 minutes
    Data=np.loadtxt('data.csv',delimiter=',')
    Data =Data.T
    X,y = Data[:-1,:]/255.,Data[-1,:].astype(int)%2 #rescale the image, convert the labels to 0s and 1s (For even and odd integers)
    Data = None

    # Extract testing data
    X_test = X[:,tsize:]
    y_test = y[tsize:]
    print("processed dataset")
    return X,y,X_test,y_test
#----------------------------

def snm_test(X,y,X_test,y_test,omethod,input=(None)):
    """Train single neuron model with input images and labels (i.e. use data in X and y), then compute and return testing error in test_error
    using X_test, y_test. The fitting parameters obtained via training should be returned in the 1-d array, fvec_f
    X: training image data, should be 784 x d with 1<=d<=60000
    y: training image labels, should contain d elements
    X_test,y_test: should be set as in read_data above
    omethod=1: use l-bfgs-b optimizer
    omethod=2: use stochastic gradient descent
    input: tuple, set if and as needed
    """
    n = X.shape[0]
    fvec = np.random.randn(n+1) #initial fitting parameters
    #Add code to train SNM and evaluate testing test_error
    
    d=70000-y_test.shape[0]
    
    nm.nm_x=X[:,0:d]
    nm.nm_y=y[0:d]
    
    def cost(w):
        return nm.snmodel(w,d,n)[0]
    def cgrad(w):
        return nm.snmodel(w,d,n)[1]

    t0=time.time()      #start of training
    
    if omethod==1:
        fvec_f=scipy.optimize.fmin_l_bfgs_b(cost,fvec,cgrad)[0]
    if omethod==2:
        fvec_f=nm.sgd(fvec,n,0,d,0.01)
    
    t1=time.time()      #end of training
    tr_time=t1-t0       #total training time
    print("training time: "+str(np.round(tr_time,2))+" seconds") 
    
    zvec=np.matmul(fvec_f[0:n],X_test)+fvec_f[n]
    avec=1/(1+np.exp(-zvec))        #vectorized calculation of activation
    pred=np.round(avec)
    
    n_correct=sum(pred==y_test)

    test_error=1-n_correct/(70000-d)
    print("test error: "+str(np.round(100*test_error,1))+"%")
    """
    The most computationally expensive part of the program is calculation of cost, derivatives and optimization of
    fitting parameters. Writing these bits in a lower level language such as Fortran, which is better suited for lengthy matrix calculations 
    makes to whole program faster than a program written solely in Python, at the same time matching the readability
    and convenience of the latter. 
  """
    output = tr_time #output tuple, modify as needed
    return fvec_f,test_error,output  
#--------------------------------------------

def nnm_test(X,y,X_test,y_test,m,omethod,input=(None)):
    """Train neural network model with input images and labels (i.e. use data in X and y), then compute and return testing error (in test_error)
    using X_test, y_test. The fitting parameters obtained via training should be returned in the 1-d array, fvec_f
    X: training image data, should be 784 x d with 1<=d<=60000
    y: training image labels, should contain d elements
    X_test,y_test: should be set as in read_data above
    m: number of neurons in inner layer
    omethod=1: use l-bfgs-b optimizer
    omethod=2: use stochastic gradient descent
    input: tuple, set if and as needed
    """
    n = X.shape[0]
    fvec = np.random.randn(m*(n+2)+1) #initial fitting parameters

    #Add code to train NNM and evaluate testing error, test_error
    d=70000-y_test.shape[0]
    
    nm.nm_x=X[:,0:d]
    nm.nm_y=y[0:d]
    
    def cost(w):
        return nm.nnmodel(w,n,m,d)[0]
    def cgrad(w):
        return nm.nnmodel(w,n,m,d)[1]

    t0=time.time()      #start of training

    if omethod==1:
        fvec_f=scipy.optimize.fmin_l_bfgs_b(cost,fvec,cgrad)[0]
    if omethod==2:
        fvec_f=nm.sgd(fvec,n,m,d,0.01)
        
    t1=time.time()      #end of training  
    tr_time=t1-t0       #total training time
    print("training time: "+str(np.round(tr_time,2))+" seconds")
    
    #unpack fvec_f
    w_inner=np.zeros([m,n])
    for i1 in range(n):
        j1 = i1*m
        w_inner[:,i1] = fvec_f[j1:j1+m] #inner layer weight matrix
    b_inner = fvec_f[n*m:n*m+m] #inner layer bias vector
    w_outer = fvec_f[n*m+m:n*m+2*m] #output layer weight vector
    b_outer = fvec_f[n*m+2*m] #output layer bias
    
    #calculate activations of inner and outer layers
    zvec_in=np.matmul(w_inner,X_test)
    for k in range(70000-d):
        zvec_in[:,k]+=b_inner
    avec_in=1/(1+np.exp(-zvec_in))
    zvec_out=np.matmul(w_outer,avec_in)+b_outer
    avec_out=1/(1+np.exp(-zvec_out))
    
    pred=np.round(avec_out)

    n_correct=sum(pred==y_test)
  
    test_error=1-n_correct/(70000-d)
    print("test error: "+str(np.round(100*test_error,1))+"%")
    
    output = tr_time #output tuple, modify as needed
    return fvec_f,test_error,output
#--------------------------------------------

def nm_analyze(X,y,X_test,y_test,omethod):
    """ Analyze performance of single neuron and neural network models
    on even/odd image classification problem
    Add input variables and modify return statement as needed.
    Should be called from
    name==main section below
    """
    
    """There are m*(n+2)+1 parameter values to be optimized based on training data
        we want to avoid a situation in which there is too many parameters, because
        the model would most likely overfit the training data and consequently perform
        poorly on the unseen test data. Another practical limit is training time, trying 
        m=40 and using sgd I hoped to get a jump in error rate, but unfortunately, after 45mins of training,
        test error rate decreased to 3%, and 2.4% after 12 minutes for l-bfgs-b. According to the plot, 
        with sgd optimization the training time is linearly dependent on m, for l-bfgs-b the relationship is rather unclear.
        For both methods the error rates are roughly equal with a significant time advantage for l-bfgs-b.
        After a little research a possible explanation why l-bfgs-b performs better is the fact that this algorithm
        additionally approximates Hessian matrix to improve the search for optimal values, thus converges more quickly.
        Nevertheless, it does not mean that l-bfgs-b is universally better, as it was almost 2x slower for the single neuron model.
        
            omethod=sgd                         l-bfgs-b
        training time: 74.83 seconds        138.12 seconds  m=0
        test error:     9.4%                9.3%
        
        training time: 445.51 seconds       207.84 seconds  m=3
        test error:     5.0%                6.4%
        
        training time: 585.76 seconds       257.63 seconds  m=6
        test error:     3.7%                3.9%
        
        training time: 717.8 seconds        256.85 seconds  m=9
        test error:     3.5%                3.3%
        
        training time: 880.49 seconds       271.43 seconds  m=12
        test error:     3.5%                3.3%
    """
    ac_t=np.zeros([5,2])
    ac_t[0,:]=snm_test(X,y,X_test,y_test,omethod)[1:]
    ac_t[1,:]=nnm_test(X,y,X_test,y_test,3,omethod)[1:]
    ac_t[2,:]=nnm_test(X,y,X_test,y_test,6,omethod)[1:]
    ac_t[3,:]=nnm_test(X,y,X_test,y_test,9,omethod)[1:]
    ac_t[4,:]=nnm_test(X,y,X_test,y_test,12,omethod)[1:]
    
    
    
    return ac_t
#--------------------------------------------

def display_image(X):
    """Displays image corresponding to input array of image data"""
    n2 = X.size
    n = np.sqrt(n2).astype(int) #Input array X is assumed to correspond to an n x n image matrix, M
    M = X.reshape(n,n)
    plt.figure()
    plt.imshow(M)
    return None
#--------------------------------------------
#--------------------------------------------


if __name__ == '__main__':
    #The code here should call analyze and generate the
    #figures that you are submitting with your code
    [X,y,X_test,y_test]=read_data(60000)
    output = nm_analyze(X,y,X_test,y_test,1)
    mvec=[0, 3, 6, 9, 12]
    fig = plt.figure()
    plt.plot(mvec,output[:,0],'o')
    plt.title('Test error vs. number of neurons in hidden layer //Aleksander Grzyb nm_analyze()')
    plt.xlabel('m')
    plt.ylabel('error')
    fig.savefig('error.png')
    
    fig = plt.figure()
    plt.plot(mvec,output[:,1],'o')
    plt.title('Training time vs. number of neurons in hidden layer //Aleksander Grzyb nm_analyze()')
    plt.xlabel('m')
    plt.ylabel('time [s]')
    fig.savefig('time.png')